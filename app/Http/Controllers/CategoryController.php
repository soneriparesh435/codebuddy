<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Category;

class CategoryController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function manageCategory()
    {

        $categories = Category::where('parent_id', '=', 0)->get();
        $allCategories = Category::pluck('title', 'id')->all();

        return view('category', compact('categories', 'allCategories'));
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function addCategory(Request $request)
    {

        $this->validate($request, [
            'title' => 'required',
        ]);

        $input = $request->all();

        $input['parent_id'] = empty($input['parent_id']) ? 0 : $input['parent_id'];

        Category::create($input);
        return back()->with('success', 'New Category added successfully.');
    }
    public function listCategory()
    {

        $allCategories = Category::pluck('title', 'id')->all();
    }
    public function editCategory(Request $request)
    {
        $category = Category::whereId($request->id)->update(['title' => $request->title]);
        return true;
    }
    public function deletecategory(Request $request)
    {
        $category = Category::whereId($request->id)->first();
        $this->allChild($category);
        $category->delete();
        return back();
    }
    public function allChild($category)
    {
        $category->load('childs');
        if ($category->childs->count() > 0) {
            foreach ($category->childs as $key => $value) {
                $this->allChild($value);
            }
        }
        $category->delete();
    }
   
}
