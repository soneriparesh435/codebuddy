<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
              $this->middleware('guest')->except('logout');
        
    }
    public function logout(Request $request){
        Auth::logout();
        if( Auth::guard('user')->check()){
            Auth::guard('user')->logout();
        }
        Auth::logout();
        $request->session()->invalidate();
        return redirect('/login');
        
    }
    public function redirectTo()
    {
        if (auth()->user()->user_type == 'admin') {
            return '/admin-dashboard';
        } else {
            return '/home';
        }
    }
}
