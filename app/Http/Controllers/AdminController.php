<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
 public function index()
 {

    return view('admin-dashboard',['users' => User::where('user_type','user')->get()]);
 }
 public function loginUserDash(Request $request)
 {
     return redirect()->route('home',['id' => $request->id]);
     
 }
}
