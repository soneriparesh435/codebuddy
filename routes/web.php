<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('admin-to-user-dashboard', [App\Http\Controllers\AdminController::class, 'loginUserDash'])->name('admin-to-user-dashboard');
Route::get('edit-category', [App\Http\Controllers\CategoryController::class, 'editCategory'])->name('edit-category');
Route::get('delete-category', [App\Http\Controllers\CategoryController::class, 'deletecategory'])->name('delete-category');
Route::get('admin-dashboard', [App\Http\Controllers\AdminController::class, 'index'])->name('admin-dashboard');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/category-tree-view', [App\Http\Controllers\CategoryController::class, 'manageCategory'])->name('category.view');
Route::post('/add-category', [App\Http\Controllers\CategoryController::class, 'addCategory'])->name('add.category');

