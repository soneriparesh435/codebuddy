@extends('layouts.app')

@section('content')

 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> 
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
 <link href="{{ asset('/css/treeview.css')}}" rel="stylesheet">
<div class="container">
	<div class="panel panel-primary">
		<div class="panel-heading">Admin Dashboard</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-6">
					<h3>Category List</h3>
					<ul id="tree1">
						@foreach($categories as $category)
						<li>
							{{ $category->title }}
							@if(count($category->childs))
							@include('child_category',['childs' => $category->childs])
							@endif
						</li>
						@endforeach
					</ul>
				</div>
				<div class="col-md-6">
					<h3>Add New Category</h3>


					{!! Form::open(['route'=>'add.category']) !!}


					@if ($message = Session::get('success'))
					<div class="alert alert-success alert-block">
						<button type="button" class="close" data-dismiss="alert">×</button>
						<strong>{{ $message }}</strong>
					</div>
					@endif


					<div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
						{!! Form::label('Title:') !!}
						{!! Form::text('title', old('title'), ['class'=>'form-control', 'placeholder'=>'Enter Title']) !!}
						<span class="text-danger">{{ $errors->first('title') }}</span>
					</div>


					<div class="form-group {{ $errors->has('parent_id') ? 'has-error' : '' }}">
						{!! Form::label('Category:') !!}
						{!! Form::select('parent_id',$allCategories, old('parent_id'), ['class'=>'form-control', 'placeholder'=>'Select Category']) !!}
						<span class="text-danger">{{ $errors->first('parent_id') }}</span>
					</div>

					<br>
					<div class="form-group">
						<button class="btn btn-success">Add New</button>
					</div>


					{!! Form::close() !!}


				</div>
				
			</div>
			<div class="row">
			<div class="col-md-12">
			  <h3>Category List <b>(Double Tap For Update)</b></h3>
					<table class="table table-striped">
						<thead>
							<tr>
								<th scope="col">Id</th>
								<th scope="col">Title</th>
								<th scope="col">Child Category</th>
								<th scope="col" colspan="2">Action</th>
							
							</tr>
						</thead>
						@foreach($categories as $category)
						<tbody>
							<td>{{ $category->id }}</td>
							<td> <a class="dowblieeee" href="# " data-id="{{ $category->id }}"> {{ $category->title }}</a></td>
							<td>
								
							@if(count($category->childs))
							@include('simple_category',['childs' => $category->childs])
							@endif
						
							</td>
							<td> <a href="{{ route('delete-category',['id' =>  $category->id])}}" class="btn btn-danger">delete</a></td>
							
						</tbody>
						@endforeach
					</table>


			</div>
			</div>


		</div>
	</div>
</div>
<script src="/js/treeview.js"></script>
<script>
	$('.dowblieeee').click(function(){
		var treeval = $(this).text();
		var id = $(this).data('id');
		Swal.fire({
  title: 'Update Category',
  input: 'text',
  inputValue: treeval,

  inputAttributes: {
    autocapitalize: 'off'
  },
  showCancelButton: true,
  confirmButtonText: 'Update',
  showLoaderOnConfirm: true,
  preConfirm: (title) => {

    return fetch(`{{ route('edit-category')}}?id=${id}&title=${title}`)
      .then(response => {
        if (!response.ok) {
          throw new Error(response.statusText)
        }
        return response.json()
      })
      .catch(error => {
        Swal.showValidationMessage(
          `Request failed: ${error}`
        )
      })
  },
  allowOutsideClick: () => !Swal.isLoading()
}).then((result) => {
  if (result.isConfirmed) {
    Swal.fire({
  position: 'top-end',
  icon: 'success',
  title: 'Title Has Been Updated.',
  showConfirmButton: false,
  timer: 1500
})
location.reload();
  }
})
	});
</script>
@endsection