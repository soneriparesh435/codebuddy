<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $noOfUsers = [
            ['name'=>'Admin','email'=>'admin@gmail.com','user_type' => 'admin', 'password' => bcrypt('admin@123')],
            ['name'=>'user','email'=>'user@gmail.com','user_type' => 'user', 'password' => bcrypt('12345678')]
        ];
        
        User::insert($noOfUsers);
    }
}
